﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HermiteManager : MonoBehaviour {

    public Vector3 P0;
    public Vector3 P1;
    public Vector3 V0;
    public Vector3 V1;

    [Range(5,200)]
    public int Ndiscret;

    private LineRenderer line;
    private Matrix4x4 matrixHermite;

	// Use this for initialization
	void Start () {
        line = GetComponent<LineRenderer>();
        matrixHermite = new Matrix4x4();
        matrixHermite.SetColumn(0, new Vector4(2, -3, 0, 1));
        matrixHermite.SetColumn(1, new Vector4(-2, 3, 0, 0));
        matrixHermite.SetColumn(2, new Vector4(1, -2, 1, 0));
        matrixHermite.SetColumn(3, new Vector4(1, -1, 0, 0));
	}
	
	// Update is called once per frame
	void Update () {
        int n = 0;
        line.positionCount = Ndiscret + 1;
        foreach(Vector3 position in Hermite())
        {
            line.SetPosition(n, position);
            ++n;
        }
    }


    List<Vector3> Hermite()
    {
        List<Vector3> curve = new List<Vector3>();

        Matrix4x4 parametersX = Matrix4x4.zero;
        Matrix4x4 parametersY = Matrix4x4.zero;
        Matrix4x4 parametersZ = Matrix4x4.zero;
        parametersX.SetColumn(0, new Vector4(P0.x, P1.x, V0.x, V1.x));
        parametersY.SetColumn(0, new Vector4(P0.y, P1.y, V0.y, V1.y));
        parametersZ.SetColumn(0, new Vector4(P0.z, P1.z, V0.z, V1.z));


        for (int n = 0; n <= Ndiscret; ++n )
        {
            float u = n / (float)Ndiscret;

            Matrix4x4 matrixU = Matrix4x4.zero;
            matrixU.SetRow(0, new Vector4(Mathf.Pow(u, 3), Mathf.Pow(u, 2), u, 1));

            Vector3 position = new Vector3(
                (matrixU * matrixHermite * parametersX).GetRow(0).x,
                (matrixU * matrixHermite * parametersY).GetRow(0).x,
                (matrixU * matrixHermite * parametersZ).GetRow(0).x

            );
            curve.Add (position); 
        }

        return curve;
    }
}
