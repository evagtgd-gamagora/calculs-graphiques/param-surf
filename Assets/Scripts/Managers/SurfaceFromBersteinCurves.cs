﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceFromBersteinCurves : MonoBehaviour {

    public List<MultiBersteinManager> multiBersteins;
    public bool stopUpdate = false;

    private List<List<Vector3>> lines;
    private Mesh mesh;

    private void Start()
    {
        StartCoroutine(SurfaceCoroutine());
    }

    IEnumerator SurfaceCoroutine()
    {
        yield return new WaitForSeconds(0.2f);
        while (!stopUpdate)
        {
            CreateSurface();
            yield return new WaitForSeconds(0.05f);
        }
    }

    void CreateSurface()
    {
        GetLinesFromMultiBersteinManager();

        if (CheckLines())
        {
            CreateMeshFromLines();
            AttachMeshToFilter();
        }  
    }

    void GetLinesFromMultiBersteinManager()
    {
        lines = new List<List<Vector3>>();

        foreach(MultiBersteinManager mb in multiBersteins)
        {
            List<Vector3> line = new List<Vector3>();

            for (int i = 0; i < mb.bersteinLines.Count; i++)
            {
                Line_InEditor l = mb.bersteinLines[i];

                if (i == 0)
                    line.AddRange(l.points);
                else
                {
                    for (int j = 1; j < l.points.Count; j++)
                    {
                        line.Add(l.points[j]);
                    }
                }
            }

            lines.Add(line);
        }
    }

    bool CheckLines()
    {
        if (lines.Count < 2)
            return false;

        int n = lines[0].Count;

        if (n < 2)
            return false;

        foreach (List<Vector3> line in lines)
            if (line.Count != n)
                return false;

        return true;
    }

    void CreateMeshFromLines()
    {
        //Assumption : lines have the same number of points
        int nLinePoints = lines[0].Count;

        int nVertices  = lines.Count * nLinePoints;
        int nTriangles = 2 * (lines.Count - 1) * (nLinePoints - 1);

        Vector3[] vertices = new Vector3[nVertices];
        int[] triangles = new int[3 * nTriangles];

        int index = 0;
        for (int i = 0; i < lines.Count; i++)
            for (int j = 0; j < nLinePoints; j++)
            {
                vertices[index] = lines[i][j];
                ++index;
            }

        index = 0;
        for (int i = 0; i < lines.Count - 1; i++)
            for (int j = 0; j < nLinePoints - 1; j++)
            {
                triangles[index]     = nLinePoints * i          + j;
                triangles[index + 1] = nLinePoints * (i + 1)    + j;
                triangles[index + 2] = nLinePoints * (i + 1)    + (j + 1);

                triangles[index + 3] = nLinePoints * i          + j;
                triangles[index + 4] = nLinePoints * (i + 1)    + (j + 1);
                triangles[index + 5] = nLinePoints * i          + (j + 1);

                index += 6;
            }

        mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = triangles;

        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
        mesh.RecalculateBounds();
    }

    void AttachMeshToFilter()
    {
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = mesh;
    }
}
