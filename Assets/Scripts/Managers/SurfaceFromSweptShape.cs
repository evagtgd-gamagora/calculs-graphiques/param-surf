﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceFromSweptShape : MonoBehaviour
{
    public MultiBersteinManager mb_line;
    public MultiBersteinManager mb_shape;

    public bool withRotation = false;
    public bool withTorsion = true;
    public bool withAnimation = false;

    public bool stopUpdate = false;

    private List<Vector3> line;
    private List<Vector3> shape;
    private Mesh mesh;

    private List<Vector3> tangents;
    private List<Vector3> normals;
    private List<Vector3> binormals;


    private void Start()
    {
        StartCoroutine(SurfaceCoroutine());
    }

    IEnumerator SurfaceCoroutine()
    {
        yield return new WaitForSeconds(0.05f);
        while (!stopUpdate)
        {
            CreateSurface();
            yield return new WaitForSeconds(0.05f);
        }
    }

    void CreateSurface()
    {
        GetLineFromMultiBersteinManager();
        GetShapeFromMultiBersteinManager();

        if (CheckLineAndShape())
        {
            CreateMeshFromLines();
            AttachMeshToFilter();
        }
    }

    void GetLineFromMultiBersteinManager()
    {
        line = new List<Vector3>();

        for (int i = 0; i < mb_line.bersteinLines.Count; i++)
        {
            Line_InEditor l = mb_line.bersteinLines[i];

            if (i == 0)
                line.AddRange(l.points);
            else
            {
                for (int j = 1; j < l.points.Count; j++)
                {
                    line.Add(l.points[j]);
                }
            }
        }
    }

    void GetShapeFromMultiBersteinManager()
    {
        shape = new List<Vector3>();

        for (int i = 0; i < mb_shape.bersteinLines.Count; i++)
        {
            Line_InEditor l = mb_shape.bersteinLines[i];

            if (i == 0)
                shape.AddRange(l.points);
            else
            {
                for (int j = 1; j < l.points.Count; j++)
                {
                    shape.Add(l.points[j]);
                }
            }
        }
    }

    bool CheckLineAndShape()
    {
        if (line.Count < 2)
            return false;

        if (shape.Count < 2)
            return false;

        return true;
    }

    void CreateMeshFromLines()
    {
        int nVertices = line.Count * shape.Count + 2;
        int nTriangles = 2 * line.Count * shape.Count;

        Vector3[] vertices = new Vector3[nVertices];
        int[] triangles = new int[3 * nTriangles];

        int index = 0;
        if (!withRotation)
        {
            for (int i = 0; i < line.Count; i++)
            {
                Vector3 translation = line[i] - line[0];

                for (int j = 0; j < shape.Count; j++)
                {
                    vertices[index] = shape[j] + translation;
                    ++index;
                }
            }
        }
        else
        {
            CalculateTangentsNormalsBinormals();

            for (int i = 0; i < line.Count; i++)
            {

                for (int j = 0; j < shape.Count; j++)
                {
                    Vector3 reference = shape[j] - line[0];
                    float shapeTangentComponent = Vector3.Dot(reference, tangents[0]);
                    float shapeNormalComponent = Vector3.Dot(reference, normals[0]);
                    float shapeBinormalComponent = Vector3.Dot(reference, binormals[0]);

                    vertices[index] = line[i] + tangents[i] * shapeTangentComponent + normals[i] * shapeNormalComponent + binormals[i] * shapeBinormalComponent;

                    if(withAnimation)
                    {
                        Quaternion animationRotation = Quaternion.Euler(180 * tangents[i] * (Mathf.PingPong(0.5f * Time.time, 2) - 1));
                        vertices[index] = line[i] + animationRotation * (vertices[index] - line[i]);
                    }

                    ++index;
                }
            }
        }

        vertices[index] = line[0];
        ++index;
        vertices[index] = line[line.Count - 1];

        index = 0;
        for (int i = 0; i < line.Count - 1; i++)
            for (int j = 0; j < shape.Count; j++)
            {
                triangles[index]     = shape.Count * i          + j;
                triangles[index + 1] = shape.Count * (i + 1)    + j;
                triangles[index + 2] = shape.Count * (i + 1)    + (j + 1) % shape.Count;

                triangles[index + 3] = shape.Count * i          + j;
                triangles[index + 4] = shape.Count * (i + 1)    + (j + 1) % shape.Count;
                triangles[index + 5] = shape.Count * i          + (j + 1) % shape.Count;

                index += 6;
            }

        for (int j = 0; j < shape.Count; j++)
        {
            triangles[index]     = j;
            triangles[index + 1] = (j + 1) % shape.Count;
            triangles[index + 2] = nVertices - 2;

            triangles[index + 3] = nVertices - 1;
            triangles[index + 4] = shape.Count * (line.Count - 1) + (j + 1) % shape.Count;
            triangles[index + 5] = shape.Count * (line.Count - 1) + j;

            index += 6;
        }

        mesh = new Mesh();
        mesh.vertices = vertices;
        mesh.triangles = triangles;

        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
        mesh.RecalculateBounds();
    }

    void CalculateTangentsNormalsBinormals()
    {
        tangents = new List<Vector3>();
        normals = new List<Vector3>();
        binormals = new List<Vector3>();

        for (int i = 0; i < line.Count; ++i)
        {
            int start = Mathf.Max(0, i - 1);
            int end = Mathf.Min(line.Count - 1, i + 1);

            Vector3 tangent = line[end] - line[start];
            tangent = tangent.normalized;

            tangents.Add(tangent);
        }

        for (int i = 0; i < line.Count; ++i)
        {
            int start = Mathf.Max(0, i - 1);
            int end = Mathf.Min(line.Count - 1, i + 1);

            int start_b = Mathf.Max(0, i - 2);
            int end_b = Mathf.Min(line.Count - 1, i + 2);

            Vector3 binormal;
            if (withTorsion)
                binormal = Vector3.Cross(line[end] - line[start], (line[end_b] - line[start]) - (line[end] - line[start_b]));
            else
                binormal = Vector3.ProjectOnPlane(Vector3.up, tangents[i]);
            binormal = binormal.normalized;

            Vector3 normal = Vector3.Cross(binormal, tangents[i]);
            normal = normal.normalized;

            //if (withAnimation)
            //{
            //    Quaternion animationRotation = Quaternion.Euler(90 * tangents[i] * (Mathf.PingPong(2 * Time.time, 2) - 1));

            //    binormal = animationRotation * binormal;
            //    normal = animationRotation * normal;
            //}

            normals.Add(normal);
            binormals.Add(binormal);
        }
    }

    void AttachMeshToFilter()
    {
        MeshFilter meshFilter = GetComponent<MeshFilter>();
        meshFilter.mesh = mesh;
    }
}