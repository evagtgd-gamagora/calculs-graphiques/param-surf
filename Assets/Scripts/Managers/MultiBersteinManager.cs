﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[System.Serializable]
public class MultiBersteinManager : MonoBehaviour {

    public GameObject linePrefab;
    public Color originalColor_A = Color.cyan;
    public Color originalColor_B = Color.blue;
    public Color bersteinColor_A = Color.magenta;
    public Color bersteinColor_B = Color.red;

    [Range(5, 200)]
    public int Ndiscret;

    public List<PointList> originalPoints;
    

    private List<Line_InEditor> originalLines = new List<Line_InEditor>();

    [HideInInspector]
    public List<Line_InEditor> bersteinLines = new List<Line_InEditor>();

    private float movementTrigger = 0.005f;

    void Start()
    {
        //Clean between edit mode and play mode
        for (int i = transform.childCount - 1; i >= 0; --i)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }

        originalLines = new List<Line_InEditor>();
        bersteinLines = new List<Line_InEditor>();

        InitPoints();
    }

    void Update()
    {
        UpdatePoints();
    }


    void InitPoints()
    {

        //Readjust position (match lines by adjusting last position of the previous line)
        for (int i = 0; i < originalPoints.Count - 1; ++i)
        {
            int nA = originalPoints[i].points.Count;
            int nB = originalPoints[i + 1].points.Count;

            if (nA >= 2 && nB >=1)
            {
                originalPoints[i].points[nA - 1] = (originalPoints[i].points[nA - 2] + originalPoints[i + 1].points[0]) / 2;
            }
        }

        InitOriginalLines();
        InitBersteinLines();
    }



    void UpdatePoints()
    {
        //Check validity
        if(originalPoints.Count != originalLines.Count)
        {
            InitPoints();
            return;
        }

        for (int i = 0; i < originalPoints.Count; i++)
        {
            int nO = originalPoints[i].points.Count;
            int nL = originalLines[i].points.Count;

            if (i != 0)
                --nL;

            if (nO < 2 || nO != nL)
            {
                InitPoints();
                return;
            }
        }

        //Move with constraints
        //Assume that junction between lines are done with equidistance constraint
        for (int i = 0; i < originalPoints.Count - 1; i++)
        {
            int nO = originalPoints[i].points.Count;
            int nL = originalLines[i].points.Count;

            if (Mathf.Abs((originalPoints[i].points[nO - 1] - originalLines[i].points[nL - 1]).magnitude) > movementTrigger)
            {
                Vector3 movement = originalPoints[i].points[nO - 1] - originalLines[i].points[nL - 1];

                originalPoints[i].points[nO - 2] += movement;
                originalPoints[i + 1].points[0]  += movement;
            }
            else if (Mathf.Abs((originalPoints[i].points[nO - 2] - originalLines[i].points[nL - 2]).magnitude) > movementTrigger)
            {
                Vector3 movement = originalPoints[i].points[nO - 2] - originalLines[i].points[nL - 2];
                originalPoints[i + 1].points[0] -= movement;
            }
            else if (Mathf.Abs((originalPoints[i + 1].points[0] - originalLines[i + 1].points[1]).magnitude) > movementTrigger)
            {
                Vector3 movement = originalPoints[i + 1].points[0] - originalLines[i + 1].points[1];
                originalPoints[i].points[nO - 2] -= movement;
            }
        }

        DisplayOriginalLines();
        DisplayBersteinLines();
    }

    void InitOriginalLines()
    {
        foreach (Line_InEditor le in originalLines)
            DestroyImmediate(le.gameObject);

        originalLines.Clear();

        for (int i = 0; i < originalPoints.Count; ++i)
        {
            GameObject go_line = Instantiate(linePrefab, this.transform);
            Line_InEditor line = go_line.GetComponent<Line_InEditor>();
            originalLines.Add(line);

            line.color = Color.Lerp(originalColor_A, originalColor_B, (i + 1.0f) / (originalPoints.Count + 1.0f));
            line.points = new List<Vector3>();

            if (i > 0)
            {
                int n = originalPoints[i - 1].points.Count;
                line.points.Add(originalPoints[i - 1].points[n - 1]);
            }

            line.points.AddRange(originalPoints[i].points);
        }
    }

    void DisplayOriginalLines()
    {
        for (int i = 0; i < originalLines.Count; ++i)
        {
            originalLines[i].color = Color.Lerp(originalColor_A, originalColor_B, i / (originalLines.Count - 1.0f));
            originalLines[i].points = new List<Vector3>();

            if (i > 0)
            {
                int n = originalPoints[i - 1].points.Count;
                originalLines[i].points.Add(originalPoints[i - 1].points[n - 1]);
            }

            originalLines[i].points.AddRange(originalPoints[i].points);
        }
    }

    void InitBersteinLines()
    {
        foreach (Line_InEditor le in bersteinLines)
            DestroyImmediate(le.gameObject);

        bersteinLines.Clear();

        for (int i = 0; i < originalLines.Count; ++i)
        {
            GameObject go_line = Instantiate(linePrefab, this.transform);
            Line_InEditor line = go_line.GetComponent<Line_InEditor>();
            bersteinLines.Add(line);

            line.color = Color.Lerp(bersteinColor_A, bersteinColor_B, i / (originalLines.Count - 1.0f));
            line.points = Berstein(originalLines[i].points);
        }
    }

    void DisplayBersteinLines()
    {
        for (int i = 0; i < originalLines.Count; ++i)
        {
            bersteinLines[i].color = Color.Lerp(bersteinColor_A, bersteinColor_B, i / (originalLines.Count - 1.0f));
            bersteinLines[i].points = Berstein(originalLines[i].points);
        }
    }

    List<Vector3> Berstein(List<Vector3> points)
    {
        List<Vector3> curve = new List<Vector3>();


        List<float> Bcoef = new List<float>();

        float nFact = 1;
        for (int i = 2; i <= points.Count - 1; ++i)
            nFact *= i;


        for (int i = 0; i < points.Count; ++i)
        {
            float iFact = 1;
            for (int j = 2; j <= i; ++j)
                iFact *= j;

            float inFact = 1;
            for (int j = 2; j <= points.Count - 1 - i; ++j)
                inFact *= j;

            float coef = nFact / (iFact * inFact);


            Bcoef.Add(coef);
        }
        

        for (int d = 0; d <= Ndiscret; ++d)
        {
            Vector3 position = new Vector3();
            float u = d / (float)Ndiscret;
            
            for (int i = 0; i < points.Count; ++i)
                position += Bcoef[i] * points[i] * Mathf.Pow(u, i) * Mathf.Pow(1 - u, points.Count - 1 - i);

            curve.Add(position);
        }

        return curve;
    }

}
