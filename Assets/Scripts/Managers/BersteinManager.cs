﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BersteinManager : MonoBehaviour {

    public List<Vector3> originalPoints;

    [Range(5, 200)]
    public int Ndiscret;

    private LineRenderer originalLine;
    private LineRenderer bersteinLine;

    // Use this for initialization
    void Start()
    {
        originalLine = transform.GetChild(0).GetComponent<LineRenderer>();
        bersteinLine = transform.GetChild(1).GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        OriginalLine();
        BersteinLine();
    }

    void OriginalLine()
    {
        int n = 0;
        originalLine.positionCount = originalPoints.Count;
        foreach (Vector3 position in originalPoints)
        {
            originalLine.SetPosition(n, position);
            ++n;
        }
    }

    void BersteinLine()
    {
        int n = 0;
        bersteinLine.positionCount = Ndiscret + 1;
        foreach (Vector3 position in Berstein())
        {
            bersteinLine.SetPosition(n, position);
            ++n;
        }
    }


    List<Vector3> Berstein()
    {
        List<Vector3> curve = new List<Vector3>();


        List<float> Bcoef = new List<float>();

        float nFact = 1;
        for (int i = 2; i <= originalPoints.Count - 1; ++i)
            nFact *= i;


        for (int i = 0; i < originalPoints.Count; ++i)
        {
            float iFact = 1;
            for (int j = 2; j <= i; ++j)
                iFact *= j;

            float inFact = 1;
            for (int j = 2; j <= originalPoints.Count - 1 - i; ++j)
                inFact *= j;

            float coef = nFact / (iFact * inFact);


            Bcoef.Add(coef);
        }
        

        for (int d = 0; d <= Ndiscret; ++d)
        {
            Vector3 position = new Vector3();
            float u = d / (float)Ndiscret;
            
            for (int i = 0; i < originalPoints.Count; ++i)
                position += Bcoef[i] * originalPoints[i] * Mathf.Pow(u, i) * Mathf.Pow(1 - u, originalPoints.Count - 1 - i);

            curve.Add(position);
        }

        return curve;
    }

}
