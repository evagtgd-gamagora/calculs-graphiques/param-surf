﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class Line_InEditor : MonoBehaviour
{

    public List<Vector3> points;
    public Color color = Color.cyan;
    public Material matPrefab;

    void Awake()
    {
        LineRenderer lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.sharedMaterial = new Material(matPrefab);
    }

    // Update is called once per frame
    void Update()
    {
        DisplayLine();
    }

    void DisplayLine()
    {
        LineRenderer lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.sharedMaterial.SetColor("_EmissionColor", color);
        lineRenderer.positionCount = points.Count;

        int n = 0;
        foreach (Vector3 position in points)
        {
            lineRenderer.SetPosition(n, position);
            ++n;
        }
    }
}
